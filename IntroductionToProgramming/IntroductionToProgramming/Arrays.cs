﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroductionToProgramming
{
    class Arrays
    {
        //Arrays store groups of a SINGLE type of object
        //There are multiple ways of making arrays

        //An array that can hold 10 ints, currently empty
        int[] intArray = new int[10];

        //An Array with the ints 1, 2, 3 and 4 stored
        int[] anotherIntArray = new int[] { 1, 2, 3, 4 };

        //The size of these arrays can't be changed, you can only have 10 in the first and 4 in the second - Lists are better if changing the size


        //You can have multiple dimensions for arrays, picture it like a nested for loop

        string[,] names = { { "Mike", "Swanson" }, { "Amy", "Sanders" }, { "Josh", "Somedude" }, { "Outof", "Names" }, };

        public void PrintNames()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.Write(names[i, j] + " \t");
                }
                Console.WriteLine();
            }
        }



        //Lists are much more friendly to use as they can be easily modified at the cost of program performance, often the cost is worth it
        //We create Lists like so:
        List<int> newIntList = new List<int>();
        // We can make Lists of anything;
        List<Arrays> exampleList = new List<Arrays>();

        //There are many functions we can use with Lists, some common ones are below.
        public void ListFunctions()
        {
            newIntList.Add(1);
            newIntList.Remove(1);
            int numberOfElements = newIntList.Count;
            newIntList.Clear();
            newIntList.Contains(1);
        }

        //All Functions are in the Methods section here https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1?view=net-5.0

    }
}
