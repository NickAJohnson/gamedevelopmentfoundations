﻿using System;
using System.Collections.Generic;
using System.Text;


//Making decision refers to how we can get a program to 'make decisions' based on data
namespace IntroductionToProgramming
{
    class MakingDecisions
    {
        public void IfStatements()
        {
            //An if statement will execute the code if it's true
            if(true)
            {
                //code
            }

            //We can use an else statement or an else if to make more complicated decision
            int customerAge = 19;
            if(customerAge < 16)
            {
                //No Driving License
            }
            else if(customerAge < 100)
            {
                //License
            }
            else
            {
                //Unlikely
            }


            bool isAmerica = true;
            bool isSuss = false;
            //An if statement can become more advance using 'And' and 'Or' operators  '&&' and '||' respectively
            if(customerAge > 18 && isAmerica && !isSuss)
            {
                //Note we are checking if isSuss is false instead of true by using the '!'
                //Customer can buy gun
            }
        }

        public void SwitchStatement()
        {
            //A Switch Statement is similar to a long list of ifelse statements

            int totalFingers = 10;
            switch(totalFingers)
            {
                case 10:
                    Console.WriteLine("Makes Sense, kind of what I expected");
                    break;
                case 8:
                    Console.WriteLine("Oh because thumbs.... you're cool");
                    break;
                case 9:
                    Console.WriteLine("That sounds painful");
                    break;
                default:
                    Console.WriteLine("I'm not even going to ask");
                    break;
            }

            //Note that each of these require a break function to work properly
        }
    }
}
