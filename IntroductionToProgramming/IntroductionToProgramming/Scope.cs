﻿using System;
using System.Collections.Generic;
using System.Text;


//Scope refers to where in a program data can be accessed, Just because it exists in the same class doesn't mean it will always be accessible or in scope
namespace IntroductionToProgramming
{
    //Note that this is outside the class and gives an error
    int notInScope = 1;
    class Scope
    {
        //Everywhere in this class this variable can be accessed
        public int inClass = 1;


        public void AccessCheck()
        {
            if(true)
            {
                //As this was int was declared inside a compount statement '{ }', it is removed as soon as the statement finishes
                int inLoop = 1;
                //Can be accessed inside the loop
                inLoop = 1;
            }
            inClass = 1;
            //The program has no idea what this int is now that it's out of scope
            inLoop = 1;
        }
    }
}
