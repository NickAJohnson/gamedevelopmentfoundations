﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroductionToProgramming
{
    class Terminology
    {
        //***Statements***

        //Most 'Actions' in C# are called Statements and generally end with semicolons;

        //Statements have Identifiers, Operators and Values
        string identifier = "value";
        //The '=' is the operator in this case

        //Some other operators include: '+' '-' '*' '/' '%' '<<' '>>'

        //There are 3 types of statements, declaration statements, expression statements and compount statements
        //Above is both a declaration statement as it makes a new identifier and an expression statement as it gives the identifier a value
        //Compound statements come in braces '{ }' 
        //A Function is a declaration statement and a compound statement
        public void SomeFunction()
        {
            //Does nothing
        }



    }
}
