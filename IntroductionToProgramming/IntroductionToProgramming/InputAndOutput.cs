﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroductionToProgramming
{
    class InputAndOutput
    {
        //Input and Output interact with the Console
        public void BasicInputAndOutput()
        {
            //Some basic output functions
            Console.Write("Prints strings to the console");
            Console.WriteLine("This will write an entire line, essentially Console.Write() but with \n on the end");

            //'/n' prints a newline  '/t' prints a tab and use ' key infron of a '  if you need to print out: '     e.g "Jack''s drink bottle." = Jack's drink bottle

            //Console.ReadKey gets the next key entered into the console and can be stored as a value if we turn it into a string
            string oneCharactor = Console.ReadKey().ToString(); 
            //Console.ReadLine will everything until the return key is pressed and will give the result as a string
            string wholeLine = Console.ReadLine();
        }


        //This is an example of checking if a number is entered by the user
        public void CheckingAStringValue()
        {
            bool parseResult = false;
            float tempFloat = 0;
            //Continuosly loops until the TryParse function returns true
            while (parseResult == false)
            {
                parseResult = float.TryParse(Console.ReadLine(), out tempFloat);
            }
        }
    }
}
