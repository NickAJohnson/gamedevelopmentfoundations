﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IntroductionToProgramming
{
    //All of these examples have been made in their own classes, 'ClassesAndConstructors' is a class

    //A class is like making a stamp, once we've made the stamp we can slam it anywhere and make copies of what's on the stamp
    class ClassesAndConstructors
    {
        //Variables in a class are a classes  data
        int someData;
        int someOtherData;

        //We can use Constructors to Assign the Data When we make a copy of a class

        //A Constructor is a function that uses the Class Name
        public ClassesAndConstructors()
        {
            //A constructor with no parameters is called a default constructor
            //We use a default constructor to set default values
            someData = 0;
            someOtherData = 0;
        }

        public ClassesAndConstructors(int someData1, int otherData)
        {
            //This constructor takes values we can assign to our classes data
            someData = someData1;
            someOtherData = otherData;
        }


        //These constructors are type of operations, operations are ways to manipulate our class data
        //Operations are commonly called functions or methods
        public void SwapDatas()
        {
            //the void means no value is returned
            int tempData;
            tempData = someData;
            someData = someOtherData;
            someOtherData = tempData;
        }

        public bool HasData()
        {
            // By changing void to bool we now return a bool value
            if(someData != 0 || someOtherData != 0)
            {
                //Returns true if either data doesnt equal 0
                return true;
            }
            //We dont need to put this in an else statement because once a function 'returns' it is finished;
            return false;
        }
    }
}
