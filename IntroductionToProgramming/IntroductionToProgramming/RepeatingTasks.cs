﻿using System;
using System.Collections.Generic;
using System.Text;
//https://youtu.be/nt9c0UeYhFc?t=83
namespace IntroductionToProgramming
{

    //Loops let us repeat stuff in different ways with different circumstances, the most common loops are for and foreach loops.
    class RepeatingTasks
    {
        public void WhileLoops()
        {
            //While loops can be dangerous, if they never meet the end condition they'll crash your program
            //This while loop is will keep adding water until it reaches 10, note that to make a safe while loop we are incrementing the value we're checking within the while loop that way it should always exit evenutally
            int waterLevel = 0;
            while (waterLevel < 10)
            {
                waterLevel++;
            }

            //This is the worst case of a while loop, it will never leave and crashes your program instantly
            while(true)
            {
                //DoThing
            }
        }

        public void ForLoops()
        {
            //initialising a list
            List<int> myIntList = new List<int>();


            //A For loop has an initialiser, a condition and an expression
            //I always picture the initialiser as an iterator (i) but it is essentially just execture once before the loop begins
            //Conditions determine when to stop a for loop
            //Expression is what to do to the iterator each time the loop finishes, often i++ or i--; Not you don't have to use 'i' it can be called anything
            for(int i = 0; i < 10; i++)
            {
                int someInt = i;
                myIntList.Add(someInt);
            }

            //This for loop has created a list with 10 numbers from 0 to 9, we can use  a foreach loop to go through each of these

            int total = 0;
            foreach (int i in myIntList)
            {
                total += i;
            }
        }

        public void SpecialMentions()
        {
            //A Do while Loop is like a while loop that always runs once, it can have it's uses
            int iCantThinkOfAGoodExample = 3;
            do
            {
                iCantThinkOfAGoodExample++;
            } while (iCantThinkOfAGoodExample < 0);


            //You can have for loops inside of for loops often called a nested for loop

            for(int i = 0; i < 10; i++)
            {
                for(int j = 0; j < 10; j++)
                {
                    Console.WriteLine(j + 10 * i);
                    //This will print out from 0 to 99;
                }
            }
        }

    }
}
